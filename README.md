# Todo list exercise

### Install

- Install https://nodejs.org/en/
- Download archive from link provided
- Unzip file and cd into it
- run `npm install`

### Run
`node app.js`

Visit http://localhost:8080 in your browser

### High level application requirements
1. Multiple users should be able to view the shared public todo list
2. Should be able to add items
3. Should be able to delete items
4. Should be able to edit items (Missing feature)
5. Must be able to deploy in docker (Missing feature)

### Tasks
1. Add missing requirement #4 to the application
2. Add sufficient test coverage to the application and update readme on howto run the tests
3. Add missing requirement #5 to the application (Dockerfile and update readme with instructions)

### Bonus
4. Display test coverage after tests are executed
5. Find and fix the XSS vulnerability in the application. Also make sure that it wont happen again by including a test.

> ### Notes
> - Update the code as needed and document what you have done in the readme below
> - Will be nice if you can git tag the tasks by number

### Solution
Explain what you have done here and why...
Solution for #4
1. I created disabled input field to populate items when a user adds one.
2. Added edit button next to delete button to allow a user to edit the existing item
3. Added a hidden input field and a submit button to update the existing item.
3.1 This input field is shown when a user clicks on edit button
3.2 It the prepopulates the original item to be updated
3.3 Added a submit button which will then submit the form
4. When an item is updated, it then gets populated on the disabled input field.
5. The update button is using javascript to show the update input fields and prepopulates the item to be updated.
 
 
 Reasons for Solution #4
1. Reason for the above is to make the interface user friendly, not to get the user confused on how to update the item
2. Had to hide the update input fields and only show when a user clicks on update button so that they know exactly which item they are updating
3. I used javascript because it is the best language for the client side.


Solution for #5 (Docker)
1. Add the Const variables (PORT, HOST) on the app.js file. 
2. Pass the above variables on the listen.
3. Create a Dockerfile to build the docker image.
4. Add the commands to build the image (Already on the Dockerfile)
5. NOTE: PLEASE UPDATE THE DIRECTORY FOR THE APPLICATION TO ACCOMMODATE YOUR SIDE
5.1 CHANGE: WORKDIR /Documents/Melamane/interview-task/no-docker/my-todolist TO YOUR OWN DIRECTORY
6. create a .dockerignore file.
6.1 add node_modules and npm-debug.log
6.2 the above is to prevent the local modules and debug logs from being copied into the image.
7. Build the image by running this command: docker build -t melamanehermanus/node-web-app .
7.1 You can change my username to your name (melamanehermanus to dewaldjacobs)
8.Test the images by running this command : docker images
9. Run the image by this command: docker run -p 49160:8080 -d melamanehermaus/node-web-app
9.1. Use the username you have used when building the image.
10. You can check docker containers: docker ps
11. If you would like to go inside a container, run this : docker exec -it <container id> /bin/bash
11.1 The above step is not necessary for now. 
12. Test the App to get the port that docker mapped: docker ps
13. Docker must have mapped port 8080 to port 49160 inside the container at this step.
14. Call your app using curl command: curl -i localhost:49160
15. Finally check your app on the browser. :)
16. use this url: http://localhost:49160/todo
16. Test the edit, add, delete items.