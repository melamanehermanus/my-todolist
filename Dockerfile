FROM node:10
# Create app directory
WORKDIR /Documents/Melamane/interview-task/no-docker/my-todolist
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "node", "app.js" ]